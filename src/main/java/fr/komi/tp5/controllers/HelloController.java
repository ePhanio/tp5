package fr.komi.tp5.controllers;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class HelloController {

    @GetMapping
    public String hello(){
        return "Hello";
    }

    @GetMapping("/api")
    public String apiTest(){

        OkHttpClient client = new OkHttpClient();

        // Request request = new Request.Builder()
        //  .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200303T144459&")
        //  .header("Authorization", "0859e454-1a16-41bb-bcab-9d034f0594c2")
        //  .build();
        Request request = new Request.Builder()
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200303T144459&")
                .header("Authorization","Basic MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .build();

        try {
            // System.out.println(response.code());
            // System.out.println( response.body().string());
            Response response = client.newCall(request).execute();
            System.out.println(response.code());
            System.out.println(response.body().string());
            return  response.body().string();
        }catch (IOException e){
            System.err.println(":)");
            e.printStackTrace();
        }
        return "not found!!!";
    }
}
